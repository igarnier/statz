open Statz
open Numerics

let signal =
  Series.discretise
    Series.dense_float64
    (fun x -> sin (x *. 100.0))
    (Grid.regular ~start:0 ~step:0.01 ~count:1000)

let fft = Fourier.fft signal

let norm2 = Series.map Series.dense_float64 Complex.norm2 fft

let to_vectors (s : (Grid.regular, float, _) Series.t) =
  let data = Series.data s in
  let grid = Series.grid s in
  let grid = Grid.to_explicit grid in
  match grid with
  | Grid.Explicit { reals } ->
      let dim = Array.length reals in
      let xs = Array.to_list reals in
      let ys = List.init dim (fun i -> Float64.Vec.unsafe_get data i) in
      (xs, ys)

let _ =
  let handle = Gnuplot.create () in
  let (xs, ys) = to_vectors signal in
  let (xs', ys') = to_vectors norm2 in
  List.iter
    (Gnuplot.plot ~output:(Gnuplot.Output.create `Qt) handle)
    [ Gnuplot.Series.lines_xy (List.combine xs ys);
      Gnuplot.Series.lines_xy (List.combine xs' ys') ] ;
  Gnuplot.close handle

let _ =
  let grid = Grid.regular ~start:0 ~step:0.1 ~count:4000 in
  let brownian = Sde.brownian 0.0 grid in
  let r = Random.State.make [| 0x1337; 0x533D |] in
  let sample = brownian r in
  let fft = Fourier.fft sample in
  let norm2 = Series.map Series.dense_float64 Complex.norm2 fft in
  let (xs, ys) = to_vectors sample in
  let (xs', ys') = to_vectors norm2 in
  let handle = Gnuplot.create () in
  List.iter
    (Gnuplot.plot ~output:(Gnuplot.Output.create `Qt) handle)
    [ Gnuplot.Series.lines_xy (List.combine xs ys);
      Gnuplot.Series.lines_xy (List.combine xs' ys') ] ;
  Gnuplot.close handle
