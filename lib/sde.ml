open Numerics

type 'g integrator = ('g, float, Float64.Vec.t) Series.t Stats.gen

type ('g, 'd) integrand = ('g, float, 'd) Series.t Stats.gen

let brownian :
    type g. float -> g Grid.t -> (g, float, Float64.Vec.t) Series.t Stats.gen =
 fun y0 grid ->
  match grid with
  | Regular { step; count; _ } ->
      let std = sqrt step in
      let sampler = Stats.Float.gaussian ~mean:0.0 ~std in
      let state = ref y0 in
      fun rng_state ->
        let data =
          Float64.Vec.init count (fun _i ->
              let res = !state in
              state := !state +. sampler rng_state ;
              res)
        in
        Series.make Series.dense_float64 grid data
  | Explicit _ ->
      fun rng_state ->
        let data = Float64.Vec.create (Grid.count grid) in
        let _res =
          Grid.fold_increments
            (fun i incr ->
              let std = sqrt incr in
              let sampler = Stats.Float.gaussian ~mean:0.0 ~std in
              Float64.Vec.unsafe_set data i (sampler rng_state) ;
              i + 1)
            0
            grid
        in
        Series.make Series.dense_float64 grid data

(* 1d ito integral:
   t \mapsto (w \mapsto \int_a^t a(u,w) du + \int_a^t b(u,w) dB(u))
 *)
(* let ito_integral :
 *   type g d1 d2.
 *   t0: float ->
 *   integrator: g integrator ->
 *   drift: (float -> float) Stats.gen ->
 *   diffusion: (float -> float) Stats.gen ->
 *   (float, d2) Series.representation ->
 *   (g, float, d2) Series.t Stats.gen =
 *   fun ~t0 ~integrator ~drift ~diffusion repr ->
 *   Stats.generative ~sampler:begin fun () ->
 *     let integrator = Stats.sample_gen integrator in
 *     let drift = Stats.sample_gen integrand in
 *     let diffusion = Stats.sample_gen integrand in
 *     assert false
 *     (\* TODO iterate over increments *\)
 *   end *)

(* let euler_maruyama : type g. (state:float -> time:float -> float) Stats.gen -> g integrator -> float Stats.gen =
 *   fun integrand integrator ->
 *   Stats.generative ~sampler:begin fun () ->
 *     let integrator = Stats.sample_gen integrator in
 *     let integrand  = Stats.sample_gen integrand in
 *     let grid = Series.grid integrator in
 *     match grid with
 *     | Regular { step ; start ; count } ->
 *     | Explicit { times } ->
 *   end *)

(*
let ito :
  type g a.
  (g, float, a) Series.t Stats.gen ->
  (e -> float) Stats.gen ->
  float Stats.gen =
  fun driver integrand ->
  Stats.generative ~sampler:begin fun () ->
    let driver_sample = Stats.sample_gen driver in
    let integrand_sample = Stats.sample_gen integrand in
    Grid.
  end
*)
