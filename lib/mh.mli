(* ------------------------------------------------------------------------- *)
(* Metropolis-Hastings MCMC *)

module type MH_parameters = sig
  include Intf.Std

  (** Proposal Markov kernel. *)
  val proposal : t -> t Stats.Float.fin_prb

  (** Logarithm of unormalized target density. *)
  val log_weight : t -> float
end

(** Metropolis-Hastings functor. *)
module Make (X : MH_parameters) : sig
  (** The [mcmc] function produces a generative probability.
      More precisely: each sample from [mcmc ~verbosity ~initial ~burn_in]
      is a distinct Markov chain, which samples can in-turn be obtained by
      calling iteratively the closure returned by [mcmc].
      Concretely, the outer `Stats.gen` corresponds to sampling the burn-in
      while the inner corresponds to sampling from the actual chain.

      Be warned that the samples obtained from the returned by the chain
      are _not_ independent. The guarantee is that (if the proposal and
      weight are reasonable) the empirical measure will converge in law
      to the target measure. *)
  val mcmc :
    verbosity:[ `Silent | `Progress | `Trace ] ->
    initial:X.t ->
    burn_in:int ->
    X.t Stats.gen Stats.gen
end
