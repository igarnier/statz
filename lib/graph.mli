(** [Dist] handles distances between vertices. The distance between
    two vertices is the length of a shortest path between those vertices. *)
module Dist : sig
  type t = Inf | Fin of int

  val zero : t

  val one : t

  val infty : t

  val ( + ) : t -> t -> t

  val ( > ) : t -> t -> bool

  val max : 'a -> 'a -> 'a
end

module Make (Graph : Intf.Graph) : sig
  (** Undirected edges. The [equal] and [hash] function are invariant under permutation
      of the vertices in the pair encoding the edge. *)
  module Undirected_edge : sig
    type t = Graph.vertex * Graph.vertex

    val equal : Graph.V.t * Graph.V.t -> Graph.V.t * Graph.V.t -> bool

    val hash : Graph.V.t * Graph.V.t -> int
  end

  module Table : Hashtbl.S with type key = Undirected_edge.t

  (** Finite bijections between vertices and integers. *)
  module Vertex_bij : sig
    type t

    type elt = Graph.V.t

    val of_list : elt list -> t

    val nth_exn : t -> int -> elt

    val nth_opt : t -> int -> elt option

    val idx_exn : t -> elt -> int

    val idx_opt : t -> elt -> int option

    val support : t -> int

    val fold : (elt -> int -> 'a -> 'a) -> t -> 'a -> 'a
  end

  (** [adjacency_matrix g] computes the adjacency matrix of [g] as well as a
      bijection between the matrix dimensions and the vertices. The matrix is
      dense. Since the graph is undirected, it is also symmetric. *)
  val adjacency_matrix : Graph.t -> Numerics.Float64.Mat.t * Vertex_bij.t

  (** [laplacian g] computes the laplacian of the adjacency matrix,
      following the definition in 'Spectral Graph Theory', by Fan Chung Graham.
      The dimensions are the same as the adjacency matrix.
      A finite bijection is returned as well. *)
  val laplacian : Graph.t -> Numerics.Float64.Mat.t * Vertex_bij.t

  type distance_table = (Graph.vertex * Graph.vertex, Dist.t) Hashtbl.t

  (** Floyd-warshall algorithm. Complexity is O(V^3) where V is the number of
      vertices of the graph. Returns a table of all distances between pairs of
      vertices. *)
  val floyd_warshall : Graph.t -> Dist.t Table.t

  (** Computes the diameter of the graph, ie the maximum over all pair of vertices
      of the shortest-path distance between those vertices. *)
  val diameter : Graph.t -> Dist.t

  (** [volume g] computes the sum over all vertices of their degree. *)
  val volume : Graph.t -> int

  (** [degree_dist g] computes the degree distribution of [g]. *)
  val degree_dist : Graph.t -> int Stats.Float.fin_prb
end
