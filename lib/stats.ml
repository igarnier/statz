type 'a emp = 'a array

type 'a gen = Random.State.t -> 'a

let sample_emp : 'a emp -> 'a gen =
 fun data rng_state ->
  let len = Random.State.int rng_state (Array.length data) in
  data.(len)

let subsample ~n sampler : 'a gen =
 fun rng_state ->
  let counter = ref 0 in
  let rec loop rng_state =
    let res = sampler rng_state in
    incr counter ;
    if Int.equal (!counter mod n) 0 then res else loop rng_state
  in
  loop rng_state

(* Mapping *)
let map_gen : ('a -> 'b) -> 'a gen -> 'b gen =
 fun f prob rng_state -> f (prob rng_state)

let map_emp : ('a -> 'b) -> 'a emp -> 'b emp = Array.map

let truncate (type elt) (module O : Intf.Ordered with type t = elt)
    (distribution : elt emp) (p : float) =
  Array.sort O.compare distribution ;
  let len = Array.length distribution in
  let plen = int_of_float (p *. float len) in
  Array.sub distribution 0 plen

let quantile (type elt) (module O : Intf.Ordered with type t = elt)
    (distribution : elt emp) (p : float) =
  Array.sort O.compare distribution ;
  let len = Array.length distribution in
  let plen = int_of_float (p *. float len) in
  distribution.(plen)

let empirical_of_generative ~nsamples sampler rng_state =
  Array.init nsamples (fun _ -> sampler rng_state)

let empirical_of_raw_data array = array

let raw_data_empirical array = `Empirical array

module Basic_statistics (Reals : Intf.Reals) = struct
  open Reals

  (* mean/variance on empirical data *)
  let empirical_mean (distribution : Reals.t emp) =
    let ilen = one / of_int (Array.length distribution) in
    let sum = Array.fold_left ( + ) zero distribution in
    ilen * sum

  let empirical_mean_generic (type elt)
      (module L : Intf.Module with type t = elt and type R.t = Reals.t)
      (distribution : elt emp) =
    let ilen = one / of_int (Array.length distribution) in
    let sum = Array.fold_left L.add L.zero distribution in
    L.smul ilen sum

  let empirical_variance (distribution : Reals.t emp) =
    let mean = empirical_mean distribution in
    let ilen = one / of_int (Array.length distribution) in
    let sum =
      Array.fold_left
        (fun acc elt -> acc + pow (elt - mean) 2)
        zero
        distribution
    in
    ilen * sum
end

module Rational = struct
  include Fin_dist.Make (Reals.Rational)
  include Basic_statistics (Reals.Rational)
end

module Float = struct
  include Fin_dist.Make (Reals.Float)
  include Basic_statistics (Reals.Float)

  let remove_outliers ~nsigmas (dist : float emp) =
    let mean = empirical_mean dist in
    let var = empirical_variance dist in
    let std = sqrt var in
    let delta = std *. nsigmas in
    dist |> Array.to_list
    |> List.filter (fun x -> abs_float (x -. mean) <= delta)
    |> Array.of_list

  let inv_loge_2 = 1. /. log 2.

  let log2 x = log x *. inv_loge_2

  let shannon_entropy ((module Prob) : float fin_prb) =
    let acc =
      Prob.V.fold (fun _ pi acc -> acc +. (pi *. log2 pi)) Prob.weightmap 0.0
    in
    ~-.acc

  (* TODO *)
  (* let kullback_leibler : p:float fin_prb -> q:float fin_prb -> float =
   *  fun ~p ~q ->
   *   let (module P) = p in
   *   let (module Q) = q in
   *   let p_weights = P.weights in
   *   let q_weights = Q.weights in
   *   if Array.length p_weights <> Array.length q_weights then
   *     invalid_arg "kullback_leibler: inconsistent supports"
   *   else
   *     let acc = ref 0.0 in
   *     for i = 0 to Array.length p_weights - 1 do
   *       let pi = Array.unsafe_get p_weights i in
   *       let qi = Array.unsafe_get q_weights i in
   *       acc := !acc +. (pi *. log (pi /. qi))
   *     done ;
   *     ~-. (!acc) *)

  let exponential ~rate : float gen =
   fun rng_state ->
    let u = Random.State.float rng_state 1.0 in
    ~-.(log u) /. rate

  let box_muller : mean:float -> std:float -> (float * float) gen =
    let rec reject_loop rng_state =
      let u = Random.State.float rng_state 2.0 -. 1.0 in
      let v = Random.State.float rng_state 2.0 -. 1.0 in
      let s = (u *. u) +. (v *. v) in
      if s = 0.0 || s >= 1.0 then reject_loop rng_state
      else
        let weight = sqrt (-2. *. log s /. s) in
        let variate1 = u *. weight in
        let variate2 = v *. weight in
        (variate1, variate2)
    in
    fun ~mean ~std rng_state ->
      let (v1, v2) = reject_loop rng_state in
      (mean +. (std *. v1), mean +. (std *. v2))

  type gaussgen_state = Fresh | Last of float

  let gaussian ~mean ~std : float gen =
    let state = ref Fresh in
    let gen = box_muller ~mean ~std in
    fun rng_state ->
      match !state with
      | Fresh ->
          let (x1, x2) = gen rng_state in
          state := Last x2 ;
          x1
      | Last x ->
          state := Fresh ;
          x
end

module Make_fin_dist (R : Intf.Reals) = struct
  include Fin_dist.Make (R)
  include Basic_statistics (R)
end

module Pdfs = struct
  let poisson ~lambda =
    if lambda <= 0.0 then invalid_arg "Pdfs.poisson"
    else
      let log_lambda = log lambda in
      fun ~k ->
        exp
        @@ ((float_of_int k *. log_lambda) -. lambda -. Specfun.log_factorial k)
end
