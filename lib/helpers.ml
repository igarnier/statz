let rec fact (x : Z.t) =
  if Z.equal x Z.zero then Z.one
  else if Z.equal x Z.one then Z.one
  else Z.mul x (fact (Z.pred x))

let rec qpow x n =
  if n < 0 then invalid_arg "pow"
  else if n = 0 then Sparse.Rational.one
  else if n = 1 then x
  else
    let p = qpow x (n / 2) in
    if n mod 2 = 0 then Q.mul p p else Q.(mul x (mul p p))
