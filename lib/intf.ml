include Sparse.Intf

module type Linear = sig
  type t

  val zero : t

  val ( + ) : t -> t -> t

  val ( * ) : float -> t -> t
end

module type Reals = sig
  include Field

  val pow : t -> int -> t

  val uniform_unit_interval : Random.State.t -> t

  val of_int : int -> t

  val ( + ) : t -> t -> t

  val ( * ) : t -> t -> t

  val ( - ) : t -> t -> t

  val ( / ) : t -> t -> t

  val ( < ) : t -> t -> bool

  val ( > ) : t -> t -> bool

  val ( = ) : t -> t -> bool

  val to_float : t -> float

  val of_float : float -> t
end

(* We use an OCamlgraph-compatible module type to describe
   undirected graphs. We assume that all graphs are undirected
   and simple. *)
module type Graph = sig
  type t

  module V : sig
    type t

    val compare : t -> t -> int

    val hash : t -> int
  end

  type vertex = V.t

  type edge

  val nb_vertex : t -> int

  val nb_edges : t -> int

  val out_degree : t -> vertex -> int

  val mem_vertex : t -> vertex -> bool

  val mem_edge : t -> vertex -> vertex -> bool

  val succ : t -> vertex -> vertex list

  val succ_e : t -> vertex -> edge list

  val iter_vertex : (vertex -> unit) -> t -> unit

  val fold_vertex : (vertex -> 'a -> 'a) -> t -> 'a -> 'a

  val iter_edges : (vertex -> vertex -> unit) -> t -> unit

  val fold_edges : (vertex -> vertex -> 'a -> 'a) -> t -> 'a -> 'a

  val iter_succ : (vertex -> unit) -> t -> vertex -> unit

  val fold_succ : (vertex -> 'a -> 'a) -> t -> vertex -> 'a -> 'a
end

module type Fin_dist = sig
  type reals

  (** Finitely supported function *)
  type 'a fin_fun

  (** Finitely supported density *)
  type 'a fin_den = 'a fin_fun

  (** Finitely supported probability *)
  type 'a fin_prb = 'a fin_fun

  (** Kernel 'a to finitely supported measures on 'b *)
  type ('a, 'b) kernel

  (** Sample from finitely supported probability *)
  val sample_prb : 'a fin_prb -> Random.State.t -> 'a

  (** Evaluate a kernel at a point, yielding a finitely supported measure. *)
  val eval_kernel : 'a -> ('a, 'b) kernel -> ('b * reals) list

  (** Creates a finitely supported _density_ from weighted points.
      A density is not necessarily normalized.
      The underlying set needs to be totally ordered. *)
  val density :
    (module Vec with type basis = 't and type R.t = reals) ->
    ('t * reals) list ->
    't fin_den

  (** Creates a finitely supported _probability_ from weighted points.
      A probability is normalized.
      Raises [invalid_arg] if the total mass is not equal to 1.0.
      The underlying set needs to be totally ordered. *)
  val probability :
    (module Vec with type basis = 't and type R.t = reals) ->
    ('t * reals) list ->
    't fin_prb

  (** Creates a _kernel_. Needs not be normalized. *)
  val kernel :
    ?h:(module Std with type t = 'a) ->
    (module Vec with type basis = 'b and type R.t = reals) ->
    ('a -> ('b * reals) list) ->
    ('a, 'b) kernel

  (** Kernel composition. *)
  val compose :
    ?h:(module Std with type t = 'a) ->
    ('a, 'b) kernel ->
    ('b, 'c) kernel ->
    ('a, 'c) kernel

  (** Constant kernel. *)
  val constant_kernel : 'b fin_prb -> ('a, 'b) kernel

  (** Returns the total mass associated to a finitely supported density. *)
  val total_mass : 't fin_den -> reals

  (** Normalize a density to obtain a probability measure.
      This amounts to integrate the density wrt the uniform measure
      to obtain (via Riesz) a probability. *)
  val normalize : 't fin_den -> 't fin_prb

  (* Compute the mean of a finite distribution supported on an [Intf.Module].*)
  val mean_generic :
    (module Module with type t = 'elt and type R.t = reals) ->
    'elt fin_fun ->
    'elt

  (* Compute the mean of a finite distribution supported by reals. *)
  val mean : reals fin_fun -> reals

  (* Compute the variance of a finite distribution supported by reals. *)
  val variance : reals fin_fun -> reals

  (** Computes probability from empirical distribution. *)
  val fin_prb_of_empirical :
    (module Vec with type basis = 't and type R.t = reals) ->
    't array ->
    't fin_prb

  (** Finitely supported uniform distribution. *)
  val uniform :
    (module Vec with type basis = 't and type R.t = reals) ->
    't array ->
    't fin_prb

  (** Evaluates finitely supported probability on argument. Returns 0 if
      the argument is out of the support. *)
  val eval_prb : 't fin_prb -> 't -> reals

  (** Integrates a function against a finitely supported measure. *)
  val integrate : 't fin_fun -> ('t -> reals) -> reals

  (** Bayesian inverse of a kernel. Defined up to a null set wrt the
      pushforward of the prior. *)
  val inverse :
    ?h:(module Std with type t = 'u) ->
    't fin_prb ->
    ('t, 'u) kernel ->
    'u fin_prb * ('u, 't) kernel

  (** Returns the raw data underlying a finitely supported density. *)
  val raw_data_density : 't fin_den -> [> `Density of ('t * reals) list ]

  (** Returns the raw data underlying a finitely supported probability. *)
  val raw_data_probability : 't fin_prb -> [> `Probability of ('t * reals) list ]

  (** Pretty print density or finitely supported probability. *)
  val pp_fin_fun :
    (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a fin_den -> unit

  (** Biased coin. Raises an error if [bias] is not in [0,1]. *)
  val coin : bias:reals -> bool fin_prb

  (** Binomial distribution.
      [binomial p n] returns the probability of having
      [k] successes over [n] experiments, according to
      a biased coin [p]. *)
  val binomial : bool fin_prb -> int -> int fin_den
end
