module Float : Intf.Reals with type t = float

module Rational : Intf.Reals with type t = Sparse.Rational.t
