(* ------------------------------------------------------------------------- *)
(* Manipulating generative, empirical & finitely supported functions. *)

(** Generative probability (i.e. sampler) *)
type 'a gen = Random.State.t -> 'a

(** Empirical probability *)
type 'a emp

(** Sample from empirical probability *)
val sample_emp : 'a emp -> 'a gen

(** Functorial action on generative probabilities *)
val map_gen : ('a -> 'b) -> 'a gen -> 'b gen

(** Functorial action on empirical probabilities *)
val map_emp : ('a -> 'b) -> 'a emp -> 'b emp

(** Samples (under iid assumptions) [nsamples] times from [sampler], producing
    a (random!) empirical distribution. *)
val empirical_of_generative : nsamples:int -> 'a gen -> 'a emp gen

(** Returns the empirical probability distribution associated to a raw array of samples. *)
val empirical_of_raw_data : 'a array -> 'a emp

(** Returns the raw data underlying an empirical distribution. *)
val raw_data_empirical : 'elt emp -> [ `Empirical of 'elt array ]

(** Subsamples the given empirical generative distribution by sampling one out of [n]
    samples. *)
val subsample : n:int -> 'a gen -> 'a gen

(** [truncate (module O) dist p] conditions an empirical distribution
    on a totally ordered space on the lower subset containing [p] mass. *)
val truncate :
  (module Intf.Ordered with type t = 'elt) -> 'elt emp -> float -> 'elt emp

(** [quantile (module O) dist p] returns the value x that splits the support of
    the distribution into a lower subset of mass [p] and an upper subset of
    mass [1-p]. *)
val quantile :
  (module Intf.Ordered with type t = 'elt) -> 'elt emp -> float -> 'elt

module Rational : sig
  include Intf.Fin_dist with type reals = Reals.Rational.t

  (** Mean of an empirical distribution on a [Intf.Module] *)
  val empirical_mean_generic :
    (module Intf.Module with type t = 'elt and type R.t = reals) ->
    'elt emp ->
    'elt

  (** Mean of a real empirical distribution *)
  val empirical_mean : reals emp -> reals

  (** Variance of a real empirical distribution *)
  val empirical_variance : reals emp -> reals
end

module Float : sig
  include Intf.Fin_dist with type reals = Reals.Float.t

  (** Mean of an empirical distribution on a [Intf.Module] *)
  val empirical_mean_generic :
    (module Intf.Module with type t = 'elt and type R.t = reals) ->
    'elt emp ->
    'elt

  (** Mean of a real empirical distribution *)
  val empirical_mean : reals emp -> reals

  (** Variance of a real empirical distribution *)
  val empirical_variance : reals emp -> reals

  (** Remove data points which are [nsigmas] standard deviations above
      (or below) the mean. *)
  val remove_outliers : nsigmas:float -> float emp -> float emp

  val shannon_entropy : reals fin_prb -> reals

  (** Exponential distribution via inverse CDF. *)
  val exponential : rate:reals -> reals gen

  (** Gaussian distribution via Box-Muller transform.
      Returns a pair of _independent_ gaussian variates with
      prescribed mean and standard deviation. *)
  val gaussian : mean:reals -> std:reals -> reals gen
end

module Make_fin_dist (R : Intf.Reals) : sig
  include Intf.Fin_dist with type reals = R.t

  (** Mean of an empirical distribution on a [Intf.Module] *)
  val empirical_mean_generic :
    (module Intf.Module with type t = 'elt and type R.t = reals) ->
    'elt emp ->
    'elt

  (** Mean of a real empirical distribution *)
  val empirical_mean : reals emp -> reals

  (** Variance of a real empirical distribution *)
  val empirical_variance : reals emp -> reals
end

module Pdfs : sig
  (** Poisson probability density function. *)
  val poisson : lambda:float -> k:int -> float
end
