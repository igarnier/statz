module Float : Intf.Reals with type t = float = struct
  include Float

  let pp = Format.pp_print_float

  let of_int = float_of_int

  let uniform_unit_interval rng_state = Random.State.float rng_state 1.0

  let pow x n = x ** of_int n

  let neg x = ~-.x

  let ( / ) = ( /. )

  let ( * ) = ( *. )

  let ( - ) = ( -. )

  let ( + ) = ( +. )

  let ( < ) (x : float) (y : float) = x < y

  let ( > ) (x : float) (y : float) = x > y

  let ( = ) (x : float) (y : float) = x = y

  let one = 1.0

  let to_float x = x

  let of_float x = x
end

module Rational : Intf.Reals with type t = Sparse.Rational.t = struct
  let uniform_unit_interval rng_state =
    Sparse.Rational.of_float (Random.State.float rng_state 1.0)

  let pow = Helpers.qpow

  include Sparse.Rational
end
